import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms-example',
  templateUrl: './forms-example.component.html',
  styleUrls: ['./forms-example.component.scss']
})
export class FormsExampleComponent implements OnInit {

  group: FormGroup;
  errorMessage = '';
  constructor() { }

  ngOnInit() {
    this.group = new FormGroup({
      'name': new FormControl('abc',[Validators.required,Validators.pattern('0|[1-9][0-9]*')]),
      'salary': new FormControl('1234',[Validators.required])
    }) 
  }

  submitForm(){
    console.log('group', this.group);
    if(!this.group.valid){
      if(!this.group.get('name').valid){
        this.errorMessage = 'name is mandatory';
      } else if(!this.group.get('salary').valid){
        this.errorMessage = 'Salary is mandatory';
      } else {
        this.errorMessage = 'something has gone wrong';
      }
    } else {
      this.errorMessage = '';
      console.log('value',this.group.value);
      // this.group.reset();
      // this.group.disable();
      console.log('form submitted successfully')
    }
  }
}
